from django.urls import path
from .views import index

appname = 'accordApp'

urlpatterns = [
   path('', index, name = 'accordion'),
]
