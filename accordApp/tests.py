from django.test import TestCase, Client
from django.urls import resolve
from accordApp import views
from accProject.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
import os
import time

c = Client()

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_benar(self):
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_tidak_ada_url_slash_index(self):
        response = c.get('/index/')
        self.assertEqual(response.status_code, 404)

    def test_apakah_pake_fungsi_home(self):
        response = resolve('/')
        self.assertEqual(response.func, views.index)

    def test_apakah_pake_template_index_html(self):
        response = c.get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_apakah_ada_tulisan_nama(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("Carlo Tupa Indriauan", content)
    
    def test_apakah_ada_toggle_theme_switch(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("<label class='switch'", content)

    def test_apakah_ada_accordion(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("<div id='accordion' class='container-accordion'", content)
        self.assertIn("<h2 id='0'", content)
        self.assertIn("<h2 id='1'", content)
        self.assertIn("<h2 id='2'", content)

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTest, self).tearDown()