$(document).ready(function() {
    $(function() {
        $("#accordion").accordion({
            collapsible: true,
            event: "click",
            active: false,
            heightStyle: "content",
            animate: 300,
            icons:{
                header: "ui-icon-circle-plus",
                activeHeader: "ui-icon-circle-minus", 
            }
        });
    });

    $("#input-slide").click(function() {
        if ($("#input-slide").is(":checked")) {
            $('link[href="/static/css/style.css"]').attr('href', '/static/css/style1.css');
        } else {
            $('link[href="/static/css/style1.css"]').attr('href', '/static/css/style.css');
        }
    });
});